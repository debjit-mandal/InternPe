import tkinter as tk
from time import strftime
from datetime import datetime


class DigitalClock:
    def __init__(self, root):
        """Initialize the digital clock."""
        self.root = root
        self.root.title("Digital Clock")

        self.config = {
            'clock_font': ('times', 50, 'bold'),
            'date_font': ('times', 20, 'bold'),
            'bg_color': 'black',
            'fg_color': 'white',
            'padx': 10,
            'pady': 10
        }

        self.window_config = {
            'width': 400,
            'height': 200
        }

        self.center_window(self.window_config['width'], self.window_config['height'])

        self.is_24hr_format = tk.BooleanVar(value=True)
        self.update_interval_ms = 200  # Update interval in milliseconds

        self.create_labels()
        self.create_buttons()

        self.tick()

    def create_labels(self):
        """Create labels for the clock and date."""
        self.clock = self.create_label(font=self.config['clock_font'])
        self.date = self.create_label(font=self.config['date_font'])

    def create_label(self, font):
        """Create a label with the given font."""
        label = tk.Label(self.root, font=font, bg=self.config['bg_color'], fg=self.config['fg_color'],
                         padx=self.config['padx'], pady=self.config['pady'])
        label.pack(fill='both', expand=1)
        return label

    def create_buttons(self):
        """Create buttons for toggling the time format and quitting the application."""
        self.toggle_button = tk.Checkbutton(self.root, text="24 Hour Format", variable=self.is_24hr_format,
                                            command=self.toggle_time_format)
        self.toggle_button.pack()

        self.quit_button = tk.Button(self.root, text="Quit", command=self.root.quit)
        self.quit_button.pack()

    def toggle_time_format(self):
        """Toggle between 24 hour and 12 hour time formats."""
        self.tick()

    def tick(self):
        """Update the time and date display."""
        current_time = strftime('%H:%M:%S') if self.is_24hr_format.get() else strftime('%I:%M:%S %p')
        current_date = datetime.today().strftime('%B %d, %Y')  # Display date in 'Month day, Year' format

        self.clock.config(text=current_time)
        self.date.config(text=current_date)

        self.clock.after(self.update_interval_ms, self.tick)

    def center_window(self, width, height):
        """Center the window on the screen."""
        x = (self.root.winfo_screenwidth() - width) // 2
        y = (self.root.winfo_screenheight() - height) // 2

        self.root.geometry('{}x{}+{}+{}'.format(width, height, x, y))

    def update_config(self, new_config):
        """Update the configuration with the given dictionary."""
        self.config.update(new_config)


if __name__ == "__main__":
    root = tk.Tk()
    clock = DigitalClock(root)
    root.mainloop()
