import random

PLAYER_SYMBOL = "O"
COMPUTER_SYMBOL = "X"
BOARD_SIZE = 9

def print_board(board):
    print(f" {board[1]} | {board[2]} | {board[3]} ")
    print("---+---+---")
    print(f" {board[4]} | {board[5]} | {board[6]} ")
    print("---+---+---")
    print(f" {board[7]} | {board[8]} | {board[9]} ")

def check_winner(board):
    winning_combinations = [
        [1, 2, 3], [4, 5, 6], [7, 8, 9],  # rows
        [1, 4, 7], [2, 5, 8], [3, 6, 9],  # columns
        [1, 5, 9], [3, 5, 7]  # diagonals
    ]
    for combination in winning_combinations:
        if board[combination[0]] == board[combination[1]] == board[combination[2]] != " ":
            return board[combination[0]]
    if " " not in board.values():
        return "Tie"
    return None

def get_computer_move(board):
    # Check if the computer can win in the next move
    for i in range(1, BOARD_SIZE + 1):
        if board[i] == " ":
            board[i] = COMPUTER_SYMBOL
            if check_winner(board) == COMPUTER_SYMBOL:
                return i
            else:
                board[i] = " "

    # Check if the player can win in the next move and block them
    for i in range(1, BOARD_SIZE + 1):
        if board[i] == " ":
            board[i] = PLAYER_SYMBOL
            if check_winner(board) == PLAYER_SYMBOL:
                board[i] = COMPUTER_SYMBOL
                return i
            else:
                board[i] = " "

    # Choose a random available move
    available_moves = [i for i in range(1, BOARD_SIZE + 1) if board[i] == " "]
    return random.choice(available_moves)

def play_game():
    board = {i: " " for i in range(1, BOARD_SIZE + 1)}

    print("Welcome to Tic-Tac-Toe!")
    print("Instructions:")
    print_board(board)
    print(f"You are '{PLAYER_SYMBOL}' and the computer is '{COMPUTER_SYMBOL}'.")
    print("Enter the number of the square you choose.\n")

    # First move is the computer's
    board[5] = COMPUTER_SYMBOL

    while True:
        print_board(board)

        # Get the user's move
        while True:
            try:
                user_move = int(input("Enter your move (1-9): "))
                if user_move in range(1, BOARD_SIZE + 1) and board[user_move] == " ":
                    board[user_move] = PLAYER_SYMBOL
                    break
                else:
                    print("Invalid move. Try again.")
            except ValueError:
                print("Invalid input. Please enter a number.")

        # Check if the game is over
        winner = check_winner(board)
        if winner:
            print_board(board)
            if winner == "Tie":
                print("It's a tie!")
            elif winner == PLAYER_SYMBOL:
                print("Congratulations! You win!")
            else:
                print("Sorry, the computer wins.")
            break

        # Get the computer's move
        computer_move = get_computer_move(board)
        board[computer_move] = COMPUTER_SYMBOL

        # Check if the game is over
        winner = check_winner(board)
        if winner:
            print_board(board)
            if winner == "Tie":
                print("It's a tie!")
            elif winner == PLAYER_SYMBOL:
                print("Congratulations! You win!")
            else:
                print("Sorry, the computer wins.")
            break

play_game()
